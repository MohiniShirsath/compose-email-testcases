
import static org.testng.Assert.assertEquals;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class AutomationTest {
	static WebDriver driver;
	String userEmailTextbox = "identifierId";
	String userPasswordTextbox = "password";
	String nextButton = "identifierNext";
	String composeButton = "//div[contains(text(),'Compose')]";
	String sendButton = "//div[contains(@class,'T-I J-J5-Ji aoO v7 T-I-atl L3')]";
	String errorText = "//div[contains(@id,'content')]";
	String okButton = "//button[@name='ok']";
	String toRecipientTextarea = "to";
	String subject = "subjectbox";
	String messageBody = "//div[contains(@aria-label,'Message Body')]";
	String attachDocument = "//div[contains(@class,'a1 aaA aMZ')]";

	@BeforeSuite
	public void launchBrouser() {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\mohini.shirsath\\Desktop\\Automation Testcase\\Automation Test\\src\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://accounts.google.com/signin");		
	}

	@Test
	public void loginToGmail() {
		driver.findElement(By.id(userEmailTextbox)).sendKeys("mohini.shirsath4736@gmail.com");
		driver.findElement(By.id(nextButton)).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.id(userPasswordTextbox)).sendKeys("Moh83@24");
		driver.findElement(By.id(nextButton)).click();
	}

	@Test
	public void sendMailWithoutrecipeint() {
		driver.findElement(By.xpath(composeButton)).click();
		driver.findElement(By.xpath(sendButton)).click();
		String errorMessage = "Please specify at least one recipient";
		assertEquals(driver.findElement(By.xpath(errorText)).getText(), errorMessage);
		driver.findElement(By.xpath(okButton)).click();

	}

	@Test
	public void sendMailWithoutSubBody() {
		driver.findElement(By.xpath(composeButton)).click();
		driver.findElement(By.name(toRecipientTextarea)).sendKeys("shirsathmohini24@gmail.com");
		driver.findElement(By.xpath(sendButton)).click();
		String alertMessage = "Send this message without a subject or text in the body?";
		assertEquals(driver.switchTo().alert().getText(), alertMessage);
		driver.switchTo().alert().accept();
	}

	@Test
	public void sendMailWithAllDetails() {
		driver.findElement(By.xpath(composeButton)).click();
		driver.findElement(By.name(toRecipientTextarea)).sendKeys("shirsathmohini24@gmail.com");
		driver.findElement(By.name(subject)).sendKeys("Test");
		driver.findElement(By.xpath(messageBody)).sendKeys("This is test Message");
		driver.findElement(By.xpath(sendButton)).click();

	}

	@Test
	public void sendMailWithAttachment() throws AWTException {
		driver.findElement(By.xpath(composeButton)).click();
		driver.findElement(By.name(toRecipientTextarea)).sendKeys("shirsathmohini24@gmail.com");
		driver.findElement(By.name(subject)).sendKeys("Test");
		driver.findElement(By.xpath(messageBody)).sendKeys("This is test Message");
		driver.findElement(By.xpath(attachDocument)).click();
		Robot rb = new Robot();
		StringSelection str = new StringSelection(
				"C:\\Users\\mohini.shirsath\\Desktop\\Automation Testcase\\Automation Test\\src\\testDoc.doc");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str, null);
		rb.keyPress(KeyEvent.VK_CONTROL);
		rb.keyPress(KeyEvent.VK_V);
		rb.keyRelease(KeyEvent.VK_CONTROL);
		rb.keyRelease(KeyEvent.VK_V);
		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		driver.findElement(By.xpath(sendButton)).click();
	}

	@AfterSuite
	public void quiteBrowser() {
		driver.close();
	}

}
